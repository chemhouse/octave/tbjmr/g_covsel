function [num,model] = gcovsel(x,y,lv,options)
% g_covsel       - Selection of groups variables from covariance maximisation
%  
% [num,model] = gcovsel(x,y,ng,opt)
%
% INPUTS:
% x      : matrix (n,p) of predictors 
% y      : matrix (n,q) of responses
% ng     : number of groups to select
% opt    : a structure array with options (if not provided, default options
%           are used). Fields are: 
%     xscale: [{'mean'} | 'auto' | 'none' ] -> x scaling
%     yscale: ['mean' | 'auto' | 'none' ] -> y scaling
%          if case of mono response (q=1), default value is 'mean'
%          if case of multi responses (q>1), default value is 'auto'
%     selcrit: [{'threshold'} | 'maxrv' ] -> grouping method
%          if 'threshold', groups are made on double thresholding on CV and CR
%          if 'maxrv', groups are done on the maximum of RV coeff
%     tcov: [0 ... {0.8} ... 1 ] -> cov2 threshold 
%     tcor: [0 ... {0.8} ... 1 ] -> cor2 threshold
%     mode: [{'auto'}, 'prompt'] -> if mode prompt, a GUI is used to define
%     the groups
%
% OUTPUTS: 
% num : groups selected
% model : structure gathering all the results 
%
% JM Roger, INRAE, 2023
% 




if nargin==0
    options = struct('xscale','mean', 'yscale','mean','selcrit','threshold', ...
        'tcov',0.8,'tcor',0.8,'mode','auto');
    num = options;
    return
end

[nx,px] = size(x);
[ny,py] = size(y);
if nx ~= ny
  error('Number of samples do not match')
end
if nx < 2
  error('Not enough samples')
end

if nargin < 3
    error('Not enough arguments');
end

if nargin==3
    options = struct('xscale','mean', 'yscale','mean','selcrit','threshold', ...
        'tcov',0.8,'tcor',0.8);
else
    % check for struct syntax
    f = fieldnames(options);
    auth = {'xscale','yscale','selcrit','tcov','tcor','smoothing','mode','labels'};
    test = ismember(f,auth);
    if prod(test)==0
        error('one or more options are wrong. See help')
    end
end;

%%% managing default values
if isfield(options,'xscale')
    switch lower(options.xscale)
        case 'mean'
            xscale = 1;
            model.xscale = 'mean';
        case 'auto'
            xscale = 2;
            model.xscale = 'auto';
        otherwise
            xscale = 0;
            model.xscale = 'none';
    end;
else
    xscale = 1;
end;

if isfield(options,'yscale')
    switch lower(options.yscale)
        case 'mean'
            yscale = 1;
            model.yscale = 'mean';
        case 'auto'
            yscale = 2;
            model.yscale = 'auto';
        otherwise
            yscale = 0;
            model.yscale = 'none';
    end;
else
    if py>1
        yscale = 2;
        model.yscale = 'auto';
    else
        yscale = 1;
        model.yscale = 'mean';
    end;
end;


if isfield(options,'smoothing')
    smooth = options.smoothing;
else
    smooth = 3;
end

prompt = 0;
if isfield(options,'mode')
    if strcmp(options.mode,'auto')==0
        prompt = 1;
    end
end

% upper limit
lv = min( lv, rank(x) );

% initializing outputs
if nargout>1
    model.crbvar=zeros(lv,2);
    model.xdef = zeros(lv,nx,px);
    model.ydef = zeros(lv,ny,py);
    model.crbcov = zeros(lv,px);
end

% scaling
switch xscale
    case 1 
        z = x-repmat(mean(x),nx,1);
    case 2 
        z = (x-repmat(mean(x),nx,1)) ./ repmat(std(x),nx,1);
    otherwise
        z = x;
end;
switch yscale
    case 1 
        t = y-repmat(mean(y),ny,1);
    case 2 
        t = (y-repmat(mean(y),ny,1)) ./ repmat(std(y),ny,1);
    otherwise
        t = y;
end;

% total inertia
vartot=[sum(sum(z.*z)) sum(sum(t.*t))];

if prompt==1
    figure('color','white','units','normalized','position',[0.1, 0.1,0.9,0.9]);
    nh = round(sqrt(lv));
    nv = ceil(lv/nh);
end

for i=1:lv
  
    % searching covariance max
    cv = sum( (z'*t).^2 ,2);
    [cvm,nm] = max(cv);
    cvn = cv./cvm;
        
    % correlations with the selected variable
    cr=corr(z(:,nm),z)'.^2;
    
    % tracking
    if nargout > 1
        model.crbcov(i,:) = cv;
        model.crbcor(i,:) = cr;
        model.crbcovn(i,:) = cvn;
    end    
    
    switch lower(options.selcrit)
        case 'threshold'
            if prompt==0
                % sorting 
                num{i} = find(cv >= options.tcov*cv(nm) & cr >= options.tcor*cr(nm));
                [~,ind] = sort(cv(num{i}),'descend');
                num{i} = num{i}(ind);   
                % tracking
                if nargout > 1
                    model.tcov(i) = options.tcov;
                    model.tcor(i) = options.tcor;
                end    
            else
                subplot(nv,nh,i);
                grid on;
                plot(cr,cvn,'o');
                xlabel('R^2');
                ylabel('Cov^2');
                if isfield(options,'labels')
                    for k=1:px
                        if cr(k)>options.tcor/4 && cvn(k)>options.tcov/4
                            text(cr(k),cvn(k),['   ',options.labels{k}], 'Interpreter', 'none');
                        end
                    end
                end
                [xg,yg,button] = ginput(1);
                
                num{i} = find(cvn >= yg & cr >= xg);
                [~,ind] = sort(cv(num{i}),'descend');
                num{i} = num{i}(ind);  
                
                hold on;
                plot(cr(num{i}),cvn(num{i}),'or');
                hold off
                
                hline(yg,'r--');
                vline(xg,'r--');
                
                if isfield(options,'labels')
                    for k=1:length(num{i})
                        text(cr(num{i}(k)),cvn(num{i}(k)),['   ',options.labels{num{i}(k)}],'color','red', 'Interpreter', 'none');
                    end
                end      
                
                % tracking
                if nargout > 1
                    model.tcov(i) = yg;
                    model.tcor(i) = xg;
                end    

            end
            
         case 'maxrv'
            d = repmat([cr(nm);cv(nm)./cvm],1,px) - [cr,cv./cvm]';
            d = sum(d.^2);
            [d,ind] = sort(d);

            for j=1:px
                zz = z(:,ind(1:j));
                r(j) = trace(zz*zz'*t*t')/sqrt(trace(zz*zz'*zz*zz')*trace(t*t'*t*t'));
            end
            if prompt==0
                if smooth>1
                    df = savgol(r(:)',smooth,2,1);
                else
                    df = [diff(r(:));-1];
                end
                j = find(df<=0,1);
                num{i} = ind(1:j);
            else
                subplot(nv,nh,i);
                plot(r,'linewi',2);
                xlabel('#var');
                ylabel('RV');
                grid on;
                x=-1;
                p=px;
                while x<0
                    [x,y,button]=ginput(1);
                    switch button
                        case 1
                            j = round(x);
                            num{i} = ind(1:j);      
                        case 3
                            p = min(2*p,px);
                            plot(r(1:p),'linewi',2);
                            x=-1;
                        otherwise
                            p = max(round(p/2),5);
                            plot(r(1:p),'linewi',2);
                            x=-1;
                    end
                end             
                vline(x,'r--');
            end
            if nargout>1
                model.crbrv(i,:) = r;
            end
            
       otherwise
            error(sprintf('%s is not a recognized option'));
    end
    
    drawnow;
    
    % deflation of x and y 
    w = sqrt(cv(num{i})./cvm.*cr(num{i}));
    k = corrcoef(z(:,num{i}));
    r = k(:,1);
    w = w.*sign(r);
    sc = z(:,num{i})*w;

    proj = sc*inv(sc'*sc)*sc';
    z = z - proj*z;
    t = t - proj*t;

    % tracing
    if nargout > 1
        model.crbvar(i,1) = ( vartot(1)-  sum(sum(z.*z)) ) / vartot(1);
        model.crbvar(i,2) = ( vartot(2) - sum(sum(t.*t)) ) / vartot(2);
        model.xdef(i,:,:) = z;
        model.ydef(i,:,:) = t;
    end;
    
    %z(:,num{i})=0;
    
end;


